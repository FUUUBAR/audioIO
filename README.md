# audioIO

*still a WIP. Heavily based on [ofxAudioAnalyzer](https://github.com/leozimmerman/ofxAudioAnalyzer) and [Sonoscopio](https://github.com/leozimmerman/Sonoscopio).*

> Access audio stream from device, analyze it and send datas.

WebSocket events will have the following scheme: `channel-{#numer}`, i.e one event per channel.

## Build

*I do not use xcode to work with oF.*

```shell
$ make
$ make install
```
